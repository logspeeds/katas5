//////////////////////// 1 //////////////////////////////

function reverseString(string) {
    let newString = string.split('').reverse().join('');
    return newString

}

function testReverseString1() {
    let resultado = reverseString('Rafael');
    console.assert(resultado === 'leafaR', {
        "function": 'reverseString(Rafael)',
        "expected": 'leafaR',
        "result": resultado
    });
}

function testReverseString2() {
    let resultado = reverseString('Am4Nd4 & 183111');
    console.assert(resultado === '111381 & 4dN4mA', {
        "function": 'reverseString(Am4Nd4 & 183111)',
        "expected": '111381 & 4dN4mA',
        "result": resultado
    });
}

//////////////////////// 2 //////////////////////////////

function reverseSentence(sentence) {
    let newSentence = sentence.split(' ').reverse('').join(' ');
    return newSentence

}

function testReverseSentence1() {
    let resultado = reverseSentence('Rafael Penteado');
    console.assert(resultado === 'Penteado Rafael', {
        "function": 'reverseSentence(Rafael Penteado)',
        "expected": 'Penteado Rafael',
        "result": resultado
    });
}

function testReverseSentence2() {
    let resultado = reverseSentence('Am4Nd4 & 183111');
    console.assert(resultado === '183111 & Am4Nd4', {
        "function": 'reverseSentence(Am4Nd4 & 183111)',
        "expected": '183111 & Am4Nd4',
        "result": resultado
    });
}

//////////////////////// 3 //////////////////////////////

function minimumValue(Value1) {
    let newValue = Math.min.apply(null, Value1)
    return newValue
}

function testMinimumValue1() {
    let testArray = [-2, 2];
    let resultado = minimumValue(testArray);
    console.assert(resultado === -2, {
        "function": 'minimumValue(testArray)',
        "expected": NaN,
        "result": resultado
    });
}

function testMinimumValue2() {
    let testArray = [5, 2];
    let resultado = minimumValue(testArray);
    console.assert(resultado === 2, {
        "function": 'minimumValue(testArray)',
        "expected": 2,
        "result": resultado
    });
}

//////////////////////// 4 //////////////////////////////

function maximumValue(Value1) {
    let newValue = Math.max.apply(null, Value1)
    return newValue
}

function testMaximumValue1() {
    let testArray = [-2, 2];
    let resultado = maximumValue(testArray);
    console.assert(resultado === 2, {
        "function": 'maximumValue(testArray)',
        "expected": 2,
        "result": resultado
    });
}

function testMaximumValue2() {
    let testArray = [5, 2];
    let resultado = maximumValue(testArray);
    console.assert(resultado === 5, {
        "function": 'maximumValue(testArray)',
        "expected": 5,
        "result": resultado
    });
}

function testMaximumValue3() {
    let testArray = ['a', 'b'];
    let resultado = maximumValue(testArray);
    console.assert(isNaN(resultado), {
        "function": 'maximumValue(testArray)',
        "expected": NaN,
        "result": resultado
    });
}

//////////////////////// 5 //////////////////////////////

function calculateRemainder(Value1, Value2) {
    let Remainder = Value1 % Value2;
    return Remainder
}

function testCalculateRemainder1() {
    let resultado = calculateRemainder(3, 2);
    console.assert(resultado === 1, {
        "function": 'calculateRemainder(3,2)',
        "expected": 1,
        "result": resultado
    });
}

function testCalculateRemainder2() {
    let resultado = calculateRemainder(0, 5);
    console.assert(resultado === 0, {
        "function": 'calculateRemainder(testArray)',
        "expected": 0,
        "result": resultado
    });
}

//////////////////////// 6 //////////////////////////////

function distinctValues(values) {
    var newValues = values.filter(function (este, i) {
        return values.indexOf(este) === i;
    });
    return newValues
}

function testDistinctValues1() {
    let testArray = [-1, 0.5, 0.5, -1, 1, 3, 4, 5, 6, 6];
    let testResultArray = [-1, 0.5, 1, 3, 4, 5, 6];
    let resultado = distinctValues(testArray);
    console.assert(resultado.toString() === testResultArray.toString(), {
        "function": 'distinctValues(testArray)',
        "expected": [-1, 0.5, 1, 3, 4, 5, 6],
        "result": resultado
    });
}

function testDistinctValues2() {
    let testArray = [-1, 0, 0.001, -0.001, -0.001, 1];
    let testResultArray = [-1, 0, 0.001, -0.001, 1];
    let resultado = distinctValues(testArray);

    console.assert(resultado.toString() === testResultArray.toString(), {
        "function": 'distinctValues(testArray)',
        "expected": [-1, 0, 0.001, -0.001, 1],
        "result": resultado
    });
}

//////////////////////// 7 //////////////////////////////

function countValues(values) {
    const frequenceValues = {};
    values = values.replace(/[^0-9]+/g, "");

    for (let i = 0; i < values.length; i++) {
        let currentNumber = values[i];
        if (frequenceValues[currentNumber] === undefined) {
            frequenceValues[currentNumber] = 1;
        } else {
            frequenceValues[currentNumber]++;
        }
    }

    return frequenceValues
}



function testCountValues1() {
    let testArray = "1 1 1 1 1 1 1 1 1";
    let resultado = countValues(testArray);

    let newFrequenceValues = JSON.stringify(resultado)

    console.assert(newFrequenceValues === JSON.stringify({1:9}), {
        "function": 'countValues(testArray)',
        "expected": '{"1":9}',
        "result": newFrequenceValues
    });
}

function testCountValues2() {
    let testArray = "a b c d d abc";
    let resultado = countValues(testArray);

    let newFrequenceValues = JSON.stringify(resultado)

    console.assert(newFrequenceValues === JSON.stringify({}), {
        "function": 'countValues(testArray)',
        "expected": '{}',
        "result": resultado
    });
}